using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace ProjectsBoard
{
    [ApiController]
    [Route("[controller]")]
    public class ProjectsBoardController : ControllerBase
    {
        private readonly IProjectsBoardService _projectService;

        public ProjectsBoardController(IProjectsBoardService projectService)
        {
            _projectService = projectService;
        }


        [HttpGet("generate-data")]
        public IActionResult GenerateData()
        {
            try
            {
                _projectService.ExecuteGenerateData();
                return Ok("New data is generated successfully!");
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }


        [HttpGet("users")]
        public async Task<IActionResult> GetUsersByPageSize(int page, int pageSize)
        {
            try
            {
                var users = await _projectService.GetUsersByPageSizeAsync(page, pageSize);
                return Ok(users);
            }
            catch (Exception ex)
            {

                return NotFound(ex.Message);
            }
        }

        [HttpGet("single-user")]
        public async Task<IActionResult> GetUserByIdForCompare(int userId, DateTime startDate, DateTime endDate)
        {
            try
            {
                var user = await _projectService.GetUserByIdWithHours(userId, startDate, endDate);

                return user != null ? Ok(user) : Ok(0);
            }
            catch (Exception ex)
            {

                return NotFound(ex.Message);
            }
        }


        [HttpGet("users-count")]
        public async Task<IActionResult> GetAllUsersCount()
        {
            try
            {
                int users = await _projectService.GetAllUsersCount();
                return Ok(users);
            }
            catch (Exception ex)
            {

                return NotFound(ex.Message);
            }
        }


        [HttpGet("top-users")]
        public async Task<IActionResult> GetTopUsers(DateTime startDate, DateTime endDate)
        {
            try
            {
                var topUsers = await _projectService.GetTop10UsersHours(startDate, endDate);
                return Ok(topUsers);
            }
            catch (Exception ex)
            {

                return NotFound(ex.Message);
            }
        }

        [HttpGet("top-projects")]
        public async Task<IActionResult> GetTopProjects(DateTime startDate, DateTime endDate)
        {
            try
            {
                var topProjects = await _projectService.GetTop10ProjectHours(startDate, endDate);
                return Ok(topProjects);
            }
            catch (Exception ex)
            {

                return NotFound(ex.Message);
            }
        }

        [HttpGet("single-user-projects-hours")]
        public async Task<IActionResult> GetUserWithProjectsForCompare(int userId, DateTime startDate, DateTime endDate)
        {
            try
            {
                var userProject = await _projectService.GetUserByIdWithHoursForProjects(userId, startDate, endDate);

                return userProject != null ? Ok(userProject) : Ok(0);
            }
            catch (Exception ex)
            {

                return NotFound(ex.Message);
            }
        }
    }
}
