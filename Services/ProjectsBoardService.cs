﻿using Microsoft.EntityFrameworkCore;
using ProjectsBoard.Dtos;

namespace ProjectsBoard
{
    public class ProjectsBoardService : IProjectsBoardService
    {
        private readonly IProjectsBoardDbContext _dbContext;

        public ProjectsBoardService(IProjectsBoardDbContext dbContext)
        {
            _dbContext = dbContext;

            if (!_dbContext.Users.Any())
            {
                this.ExecuteGenerateData();
            }
        }

        public void ExecuteGenerateData() => _dbContext.GenerateData();

        public async Task<UserTotalHoursDto> GetUserByIdWithHours(int userId, DateTime startDate, DateTime endDate) => await _dbContext.Users
                                                                          .Where(u => u.Id == userId && u.TimeLogs.Any(tl => tl.Date >= startDate && tl.Date <= endDate))
                                                                          .Select(uh => new UserTotalHoursDto
                                                                          {
                                                                              FullName = $"{uh.FirstName} {uh.LastName}",
                                                                              TotalHours = uh.TimeLogs
                                                                                  .Where(tl => tl.Date >= startDate && tl.Date <= endDate)
                                                                                  .Sum(tl => tl.Hours)
                                                                          })
                                                                          .FirstOrDefaultAsync();
        public async Task<IEnumerable<User>> GetUsersByPageSizeAsync(int page, int pageSize) => await _dbContext.Users
                                                                          .OrderBy(u => u.Id)
                                                                          .Skip((page - 1) * pageSize)
                                                                          .Take(pageSize)
                                                                          .AsNoTracking()
                                                                          .ToListAsync();
        public async Task<int> GetAllUsersCount() => await _dbContext.Users.CountAsync();
        public async Task<IEnumerable<UserTotalHoursDto>> GetTop10UsersHours(DateTime startDate, DateTime endDate) => await _dbContext.Users
                                                                          .Where(u => u.TimeLogs.Any(tl => tl.Date >= startDate && tl.Date <= endDate))
                                                                          .Select(uh => new UserTotalHoursDto
                                                                          {
                                                                              FullName = $"{uh.FirstName} {uh.LastName}",
                                                                              TotalHours = uh.TimeLogs
                                                                                  .Where(tl => tl.Date >= startDate && tl.Date <= endDate)
                                                                                  .Sum(tl => tl.Hours)
                                                                          })
                                                                          .OrderByDescending(u => u.TotalHours)
                                                                          .Take(10)
                                                                          .ToListAsync();

        public async Task<IEnumerable<UserTotalHoursDto>> GetTop10ProjectHours(DateTime startDate, DateTime endDate) => await _dbContext.Projects
                                                                  .Where(p => p.TimeLogs.Any(tl => tl.Date >= startDate && tl.Date <= endDate))
                                                                  .Select(pj => new UserTotalHoursDto
                                                                  {
                                                                      FullName = pj.Name,
                                                                      TotalHours = pj.TimeLogs
                                                                          .Where(tl => tl.Date >= startDate && tl.Date <= endDate)
                                                                          .Sum(tl => tl.Hours)
                                                                  })
                                                                  .OrderByDescending(u => u.TotalHours)
                                                                  .Take(10)
                                                                  .ToListAsync();

        public async Task<HoursPerProjectDto> GetUserByIdWithHoursForProjects(int userId, DateTime startDate, DateTime endDate)
        {
            var user = await _dbContext.Users.Include(u => u.TimeLogs).ThenInclude(tl => tl.Project)
                       .FirstOrDefaultAsync(u => u.Id == userId);
            if (user == null)
            {
                return null;
            }

            var hoursPerProject = new HoursPerProjectDto
            {
                FullName = $"{user.FirstName} {user.LastName}",
                ProjectHours = new Dictionary<string, float>()
            };

            var timeLogsInRange = user.TimeLogs.Where(tl => tl.Date >= startDate && tl.Date <= endDate);

            foreach (var timeLog in timeLogsInRange)
            {
                var projectName = timeLog.Project.Name;
                if (!hoursPerProject.ProjectHours.ContainsKey(projectName))
                {
                    hoursPerProject.ProjectHours[projectName] = 0;
                }
                hoursPerProject.ProjectHours[projectName] += timeLog.Hours;
            }

            return hoursPerProject;
        }
    }
}
