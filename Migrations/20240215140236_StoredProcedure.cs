﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ProjectsBoard.Migrations
{
    /// <inheritdoc />
    public partial class StoredProcedure : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var command = @"CREATE PROCEDURE GenerateData
AS
BEGIN
    SET NOCOUNT ON;

-- Delete the contents of the User, Project, TimeLog tables and reset primary keys to start from 1
    IF EXISTS (SELECT 1 FROM TimeLogs)
    BEGIN
        DELETE FROM TimeLogs;
        DBCC CHECKIDENT ('TimeLogs', RESEED, 0);
    END
    ELSE
    BEGIN
        DBCC CHECKIDENT ('TimeLogs', RESEED, 1);
    END
    
    IF EXISTS (SELECT 1 FROM Users)
    BEGIN
        DELETE FROM Users;
        DBCC CHECKIDENT ('Users', RESEED, 0);
    END
    ELSE
    BEGIN
        DBCC CHECKIDENT ('Users', RESEED, 1);
    END
    
    IF EXISTS (SELECT 1 FROM Projects)
    BEGIN
        DELETE FROM Projects;
        DBCC CHECKIDENT ('Projects', RESEED, 0);
    END
    ELSE
    BEGIN
        DBCC CHECKIDENT ('Projects', RESEED, 1);
    END

    -- Generate 100 entries in the Users table
    DECLARE @i INT = 1;
    WHILE @i <= 100
	BEGIN
        DECLARE @firstName VARCHAR(50) = (SELECT TOP 1 Name FROM (VALUES ('John'), ('Gringo'), ('Mark'), ('Lisa'), ('Maria'), ('Sonya'), ('Philip'), ('Jose'), ('Lorenzo'), ('George'), ('Justin')) AS Names(Name) ORDER BY NEWID());
        DECLARE @lastName VARCHAR(50) = (SELECT TOP 1 Surname FROM (VALUES ('Johnson'), ('Lamas'), ('Jackson'), ('Brown'), ('Mason'), ('Rodriguez'), ('Roberts'), ('Thomas'), ('Rose'), ('McDonalds')) AS Surnames(Surname) ORDER BY NEWID());
        DECLARE @domain VARCHAR(50) = (SELECT TOP 1 Domain FROM (VALUES ('hotmail.com'), ('gmail.com'), ('live.com')) AS Domains(Domain) ORDER BY NEWID());
        DECLARE @email VARCHAR(100) = CONCAT(@firstName, '.', @lastName, '@', @domain);

        INSERT INTO Users (FirstName, LastName, Email)
        VALUES (@firstName, @lastName, @email);

        SET @i = @i + 1;
    END;

    -- Generate 3 projects
    INSERT INTO Projects (Name)
    VALUES ('My own'), ('Free Time'), ('Work');

    -- Generate time logs for each user
	DECLARE @userId INT;
    DECLARE @projectId INT;
    DECLARE @hours FLOAT;
    DECLARE @date DATE = GETDATE();
    DECLARE @counter INT = 1;

	DECLARE @numUsers INT = (SELECT COUNT(*) FROM Users);
	DECLARE @maxEntriesPerUser INT = 20;

	WHILE @counter <= @numUsers
	BEGIN
	    DECLARE @numEntries INT = ABS(CHECKSUM(NEWID())) % @maxEntriesPerUser + 1; -- Generate a random number of entries (1-20) per user
	
	    DECLARE @userIndex INT = ABS(CHECKSUM(NEWID())) % @numUsers + 1; -- Select a random user
	
		-- Gets the user by index
	    SELECT TOP 1 @userId = Id 
	    FROM (
	        SELECT Id, ROW_NUMBER() OVER (ORDER BY Id) AS RowNumber
	        FROM Users
	    ) AS RankedUsers
	    WHERE RowNumber = @userIndex;
	
	    DECLARE @entryCounter INT = 1;
	
	    WHILE @entryCounter <= @numEntries
	    BEGIN
	        SELECT TOP 1 @projectId = Id FROM Projects ORDER BY NEWID();
	        SET @hours = ROUND((RAND() * 7.75 + 0.25),2); -- Generate a random hour between 0.25 and 8 and round by 2 digit
	        SET @date = DATEADD(day, -1 * (ABS(CHECKSUM(NEWID())) % 365), GETDATE()); -- Generate a random date in the last year
	
	        IF NOT EXISTS (SELECT 1 FROM TimeLogs WHERE UserId = @userId AND CONVERT(DATE, Date) = @date)
	        BEGIN
	            INSERT INTO TimeLogs (UserId, ProjectId, Date, Hours)
	            VALUES (@userId, @projectId, @date, @hours);
	        END;
	
	        SET @entryCounter = @entryCounter + 1;
	    END;
	
	    SET @counter = @counter + 1;
	END;
END;
GO";

            migrationBuilder.Sql(command);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
