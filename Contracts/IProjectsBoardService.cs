﻿using ProjectsBoard.Dtos;

namespace ProjectsBoard
{
    public interface IProjectsBoardService
    {
        void ExecuteGenerateData();
        Task<IEnumerable<User>> GetUsersByPageSizeAsync(int page, int pageSize);
        Task<UserTotalHoursDto> GetUserByIdWithHours(int userId, DateTime startDate, DateTime endDate);
        Task<int> GetAllUsersCount();
        Task<IEnumerable<UserTotalHoursDto>> GetTop10UsersHours(DateTime startDate, DateTime endDate);
        Task<IEnumerable<UserTotalHoursDto>> GetTop10ProjectHours(DateTime startDate, DateTime endDate);
        Task<HoursPerProjectDto> GetUserByIdWithHoursForProjects(int userId, DateTime startDate, DateTime endDate);
    }
}
