﻿using Microsoft.EntityFrameworkCore;

namespace ProjectsBoard
{
    public interface IProjectsBoardDbContext : IDisposable
    {
        DbSet<User> Users { get; set; }
        DbSet<Project> Projects { get; set; }
        DbSet<TimeLog> TimeLogs { get; set; }

        void GenerateData();
    }
}
