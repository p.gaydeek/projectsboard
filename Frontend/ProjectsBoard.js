// Constants for table headers
const headers = {
    id: {
        element: document.getElementById('users-id'),
        sortOrder: null
    },
    firstName: {
        element: document.getElementById('users-fName'),
        sortOrder: null
    },
    lastName: {
        element: document.getElementById('users-lName'),
        sortOrder: null
    },
    email: {
        element: document.getElementById('users-email'),
        sortOrder: null
    }
};

// Initial page number and size
let pageNumber = 1;
const pageSize = 10;
let usersCount = 0;

// Buttons
const prevButton = document.getElementById('prev-page-btn');
const nextButton = document.getElementById('next-page-btn');
const generateData = document.getElementById('generate-data');
const topChoice = document.getElementById('topChoice');

// Date selection inputs
const startDateInput = document.getElementById('start-date');
const endDateInput = document.getElementById('end-date');
let startEndDatesFilled = false;

// Variable for google charts
let chart;
let chartData;
let chartOptions;
let topColumnsData = [];

// Users data array
let usersData = [];

// Initialize function
async function onInit() {
    await fetchUsersCount();
    await fetchUsers(pageNumber);
    renderGrid(usersData);
    addEventListeners();
    determineInitialSortAndButton('id', 'asc');
}

// Fetch user data for a specific page from API
async function fetchUsers(pageNumber) {
    const url = `https://localhost:44331/ProjectsBoard/users?page=${pageNumber}&pageSize=${pageSize}`;

    try {
        const response = await fetch(url);
        usersData = await response.json();
    } catch (error) {
        console.error('Error fetching users:', error);
    }
}

// Fetch total users count from API
async function fetchUsersCount() {
    const url = 'https://localhost:44331/ProjectsBoard/users-count';

    try {
        const response = await fetch(url);
        usersCount = await response.json();
    } catch (error) {
        console.error('Error fetching data:', error);
    }
}

// Fetch data for the top 10 users with the largest number of hours from API
async function fetchTopUsersData(startDate, endDate) {
    const url = `https://localhost:44331/ProjectsBoard/top-users?startDate=${startDate}&endDate=${endDate}`;

    try {
        const response = await fetch(url);
        renderBarChart(await response.json());
    } catch (error) {
        console.error('Error fetching top users data:', error);
    }
}

// Fetch data for the top 10 projects with the largest number of hours from API
async function fetchTopProjectsData(startDate, endDate) {
    const url = `https://localhost:44331/ProjectsBoard/top-projects?startDate=${startDate}&endDate=${endDate}`;

    try {
        const response = await fetch(url);
        renderBarChart(await response.json());
    } catch (error) {
        console.error('Error fetching top projects data:', error);
    }
}

// Fetch data for a specific user
async function fetchSelectedUserData(userId, startDate, endDate) {
    const url = `https://localhost:44331/ProjectsBoard/single-user?userId=${userId}&startDate=${startDate}&endDate=${endDate}`;
    try {
        const response = await fetch(url);
        const data = await response.json();
        if (data == 0) {
            alert('No data for this user in the selected dates!');
            return;
        }
        updateBarChartForComparison(data);
    } catch (error) {
        console.error('Error fetching user data:', error);
    }
}

// Fetch data for a specific user with all projects and hours
async function fetchSelectedUserDataWithProject(userId, startDate, endDate) {
    const url = `https://localhost:44331/ProjectsBoard/single-user-projects-hours?userId=${userId}&startDate=${startDate}&endDate=${endDate}`;
    try {
        const response = await fetch(url);
        const data = await response.json();
        if (data == 0 || Object.values(data.projectHours).length === 0) {
            alert('No data for this user in the selected dates!');
            return;
        }
        updateBarChartForComparison(data);
    } catch (error) {
        console.error('Error fetching user data:', error);
    }
}

// Trigger stored procedure for generating new data
async function startStoredProcedure() {
    const url = 'https://localhost:44331/ProjectsBoard/generate-data';

    try {
        const response = await fetch(url);
        return response.ok ? true : false;
    } catch (error) {
        console.error('Error generating data:', error);
    }
}

// Add click event listeners
function addEventListeners() {
    for (const column in headers) {
        const header = headers[column];
        header.element.addEventListener('click', () => handleSorting(column));
    }

    prevButton.addEventListener('click', prevPageClick);
    nextButton.addEventListener('click', nextPageClick);
    generateData.addEventListener('click', generateNewData);
    startDateInput.addEventListener('change', handleDateSelection);
    endDateInput.addEventListener('change', handleDateSelection);
    topChoice.addEventListener('change', handleDateSelection);
}

// Render the grid with users data
function renderGrid(users) {
    const usersBody = document.getElementById('users-body');
    usersBody.innerHTML = '';
    users.forEach(user => {
        const row = document.createElement('tr');
        row.innerHTML = `
              <td>${user.id}</td>
              <td>${user.firstName}</td>
              <td>${user.lastName}</td>
              <td>${user.email}</td>
              <td id="compare-cell"><button user-id="${user.id}">Compare</button></td>
          `;
        usersBody.appendChild(row);
    });

    startEndDatesFilled ? toggleActionColumn('show') : toggleActionColumn('hide');
    usersEventListeners();
}

// Function to render the bar chart with top 10 users data
function renderBarChart(currentData) {
    const topColumns = [];
    const hours = [];
    currentData.forEach(currData => {
        topColumns.push(currData.fullName);
        hours.push(currData.totalHours);
    });

    if (chart) {
        chart.destroy();
    }

    const data = {
        labels: topColumns,
        datasets: [{
            label: 'Top',
            data: hours,
            backgroundColor: [
                'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
                'rgba(153, 102, 255, 1)'
            ],
            borderWidth: 1
        },
        {
            label: 'Selected User',
            data: [],
            backgroundColor: 'rgba(255, 26, 104, 0.2)',
            borderColor: 'rgba(255, 26, 104, 1)',
            type: 'line',
            order: 1
        }]
    };

    const config = {
        type: 'bar',
        title: 'Top 10 Users by Hours',
        data
    };

    chart = new Chart(
        document.getElementById('chart-container'),
        config
    );

    chartData = data;
    chartOptions = config;
    topColumnsData = topColumns;
}

// Handle sorting of the grid
function handleSorting(column) {
    const header = headers[column];

    // Reset sort order for other headers
    for (const key in headers) {
        if (key !== column) {
            headers[key].sortOrder = null;
        }
    }

    // Determine new sort order
    header.sortOrder = (header.sortOrder === null || header.sortOrder === 'desc') ? 'asc' : 'desc';

    // Sort the data and render the grid
    sortUsersTable(column, header.sortOrder);
    renderGrid(usersData);
}

// Handle Compare button click
async function handleCompare(userId) {
    const choice = topChoice.checked;
    const startDate = startDateInput.value;
    const endDate = endDateInput.value;   

    if (startEndDatesFilled) {
        choice ? await fetchSelectedUserDataWithProject(userId, startDate, endDate) :
            await fetchSelectedUserData(userId, startDate, endDate);
    }
}

async function handleSwitch(startDate, endDate) {
    const choice = topChoice.checked;

    choice ? await fetchTopProjectsData(startDate, endDate) : await fetchTopUsersData(startDate, endDate);
}

// Sort users data in the table according to selected column and sorting order
function sortUsersTable(sortBy, sortDirection) {
    if (sortBy) {
        usersData.sort((a, b) => {
            let comparison = 0;
            if (a[sortBy] > b[sortBy]) {
                comparison = 1;
            } else if (a[sortBy] < b[sortBy]) {
                comparison = -1;
            }
            return sortDirection === 'desc' ? comparison * -1 : comparison;
        });
    }
}

// Update the bar chart for comparison with the selected user
function updateBarChartForComparison(currentUser) {
    const choice = topChoice.checked;

    if (choice) {
        let newHours = [];
        topColumnsData.forEach(column => {
            if (currentUser.projectHours[column]) {
                newHours.push(currentUser.projectHours[column]);
            }
            else {
                newHours.push(0);
            }
        });
        chartData.datasets[1].data = newHours;
    }
    else {
        const newData = new Array(10).fill(currentUser.totalHours);
        chartData.datasets[1].data = newData;
    }

    chartData.datasets[1].label = currentUser.fullName;

    chart.update();
}

// Highlight the initial sort header and buttons
function determineInitialSortAndButton(sortBy, sortDirection) {
    headers[sortBy].sortOrder = sortDirection;
    prevButton.disabled = true;
}

// Previous page click
async function prevPageClick() {
    if (pageNumber > 1) {
        if (nextButton.disabled == true) {
            nextButton.disabled = false;
        }
        pageNumber--;
        await fetchUsers(pageNumber);
        renderGrid(usersData);
    }

    prevButton.disabled = (pageNumber === 1);
}

// Next page click
async function nextPageClick() {
    pageNumber++;

    nextButton.disabled = (pageNumber * pageSize >= usersCount);

    if (prevButton.disabled == true) {
        prevButton.disabled = false;
    }
    await fetchUsers(pageNumber);
    renderGrid(usersData);
}

// Handle date selection
async function handleDateSelection() {
    const startDate = startDateInput.value;
    const endDate = endDateInput.value;

    if (startDate && new Date(startDate) > new Date()) {
        alert('Start date cannot be in the future!');
        startDateInput.value = '';
        toggleActionColumn('hide');
        return;
    }

    // Check if both dates are selected
    if (startDate && endDate) {
        if (new Date(startDate) > new Date(endDate)) {
            alert('Start date cannot be greater than end date!');
            startDateInput.value = '';
            endDateInput.value = '';
            toggleActionColumn('hide');
            return;
        }
        startEndDatesFilled = true;
        toggleActionColumn('show');
        await handleSwitch(startDate, endDate);
    }
    else{
        toggleActionColumn('hide');
    }
}

// Toggle visibility of the Action column
function toggleActionColumn(visibility) {
    const actionHeader = document.querySelector('#users-action');
    const actionCells = document.querySelectorAll('#compare-cell');

    if (visibility === 'hide') {
        actionHeader.style.display = 'none';
        actionCells.forEach(cell => {
            cell.style.display = 'none';
        });
    } else if (visibility === 'show') {
        actionHeader.style.display = 'table-cell';
        actionCells.forEach(cell => {
            cell.style.display = 'table-cell';
        });
    }
}

// Add event listeners to 'Compare' button on every user
function usersEventListeners() {
    const compareButtons = document.querySelectorAll('#compare-cell button');
    compareButtons.forEach(button => {
        button.addEventListener('click', () => {
            const userId = button.getAttribute('user-id');
            handleCompare(userId);
        });
    });
}

// Generate new data and reload page
async function generateNewData() {
    const success = await startStoredProcedure();
    if (success) {
        window.location.reload();
        await fetchUsers(pageNumber);
        renderGrid(usersData);
    }
}

onInit();