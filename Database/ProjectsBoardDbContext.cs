﻿using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace ProjectsBoard
{
    public class ProjectsBoardDbContext : DbContext, IProjectsBoardDbContext
    {
        public ProjectsBoardDbContext(DbContextOptions<ProjectsBoardDbContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<TimeLog> TimeLogs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            modelBuilder.Entity<User>()
                .HasMany(tl => tl.TimeLogs)
                .WithOne(u => u.User)
                .HasForeignKey(u => u.UserId);

            modelBuilder.Entity<Project>()
                .HasMany(tl => tl.TimeLogs)
                .WithOne(u => u.Project)
                .HasForeignKey(u => u.ProjectId);
        }

        public void GenerateData()
        {
            Database.ExecuteSqlRaw("EXEC GenerateData");
        }
    }
}
