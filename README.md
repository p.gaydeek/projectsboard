# Projects Board

This is a Projects Board application designed to manage projects and track hours worked by users. Random users are generated using a stored SQL procedure.

# How to Use
- Run the Application: Open the ProjectsBoard.sln file in Visual Studio and run the program. Swagger UI is available for testing API requests.

- Database Setup: A new database should be generated automatically upon the first execution of the application. If not, you can use the backup SQL database provided in the DatabaseBackup folder.

- Frontend Access: After setting up the backend, navigate to the Frontend folder. Open the ProjectsBoard.html file in your preferred web browser. Alternatively, you can open the entire Frontend folder in VS Code to examine its contents.

# Features
- User Tracking: Track hours worked by users on various projects.
- Swagger UI: Convenient interface for testing API requests.

# Technologies Used
- Backend: C#, ASP.NET Core, Entity Framework
- Frontend: HTML, CSS, JavaScript
- Database: SQL Server
