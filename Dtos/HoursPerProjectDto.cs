﻿namespace ProjectsBoard.Dtos
{
    public class HoursPerProjectDto
    {
        public string FullName { get; set; }
        public Dictionary<string,float> ProjectHours { get; set; } = new Dictionary<string,float>();
    }
}
