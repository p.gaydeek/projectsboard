﻿namespace ProjectsBoard
{
    public class UserTotalHoursDto
    {
        public string FullName { get; set; }
        public float TotalHours { get; set; }
    }
}
