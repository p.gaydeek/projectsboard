﻿using System.ComponentModel.DataAnnotations;

namespace ProjectsBoard
{
    public class Project
    {
        [Required]
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public IList<TimeLog> TimeLogs { get; set; } = new List<TimeLog>();
    }
}
