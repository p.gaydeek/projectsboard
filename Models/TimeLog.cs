﻿using System.ComponentModel.DataAnnotations;

namespace ProjectsBoard
{
    public class TimeLog
    {
        [Required]
        public int Id { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        public DateTime Date { get; set; }
        public float Hours { get; set; }
    }
}
