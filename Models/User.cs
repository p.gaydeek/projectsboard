﻿using System.ComponentModel.DataAnnotations;

namespace ProjectsBoard
{
    public class User
    {
        [Required]
        public int Id { get; set; }
        public string FirstName { get; set; } = string.Empty;
        public string LastName { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public IList<TimeLog> TimeLogs { get; set; } = new List<TimeLog>();
    }
}
